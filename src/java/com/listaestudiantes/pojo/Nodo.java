/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listaestudiantes.pojo;

import java.io.Serializable;

/**
 *
 * @author cloaiza
 */
public class Nodo implements Serializable{
    private Estudiante dato;
    private Nodo siguiente;

    public Nodo(Estudiante dato) {
        this.dato = dato;
    }
    
    

    public Estudiante getDato() {
        return dato;
    }

    public void setDato(Estudiante dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
    
}
